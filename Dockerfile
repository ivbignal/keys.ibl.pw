FROM python:3.8.2
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
RUN mkdir /db
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
import os
from datetime import datetime
from django import setup as setupDjango

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "KEYSAPI.settings")
setupDjango()

from API.models import Key

keys = Key.objects.all()
date = datetime.now()
with open(f'report_{date}.rip', 'w') as fo:
    for key in keys:
        num = key.num
        stat = key.get_status_display()
        who = f' by {key.teacher.name} ({key.teacher.partition})' if key.status == 'T' else ''
        fo.write(f'Key {num} is {stat}{who}\n')


import django_tables2 as tables
from .models import KeyHistory, Key

class HistoryTable(tables.Table):
    class Meta:
        model = KeyHistory
        template_name = "django_tables2/bootstrap.html"
        fields = ("datetime", "event_type", "key", "teacher")


class KeyTable(tables.Table):
    class Meta:
        model = Key
        template_name = "django_tables2/bootstrap.html"
        fields = ("num", "status", "teacher")
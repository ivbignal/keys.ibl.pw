from rest_framework import serializers

from API.models import Teacher, Key, KeyHistory, Card


class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = Teacher
        fields = ['pk', 'name', 'partition']


class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = ['pk', 'code', 'teacher']


class KeySerializer(serializers.ModelSerializer):
    class Meta:
        model = Key
        fields = ['pk', 'num', 'status', 'teacher']


class KeyHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = KeyHistory
        fields = ['pk', 'datetime', 'key', 'teacher', 'event_type']
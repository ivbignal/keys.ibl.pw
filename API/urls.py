from django.urls import include, path
from rest_framework import routers
from API import views

router = routers.DefaultRouter()
router.register(r'teacher', views.TeacherViewSet)
router.register(r'card', views.CardViewSet)
router.register(r'key', views.KeyViewSet)
router.register(r'key_history', views.KeyHistoryViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', views.KeyListView.as_view()),
    path('journal/', views.HistoryListView.as_view()),
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
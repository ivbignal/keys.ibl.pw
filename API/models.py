from django.db import models
from django.utils.translation import ugettext_lazy as _

class Teacher(models.Model):
    name = models.TextField(_("Teacher name"))
    partition = models.TextField(_("Teacher partition"))
    

    class Meta:
        verbose_name = _("Teacher")
        verbose_name_plural = _("Teachers")

    def __str__(self):
        return f'{self.name} ({self.partition})'


class Card(models.Model):
    code = models.TextField(_("Card code"))
    teacher = models.ForeignKey("Teacher", verbose_name=_("Card's teacher"), on_delete=models.CASCADE)
    

    class Meta:
        verbose_name = _("Teacher")
        verbose_name_plural = _("Teachers")

    def __str__(self):
        return f'{self.teacher} | {self.code}'


class Key(models.Model):
    KEY_STATUS = [
        ("A", "Available"),
        ("T", "Taken"),
    ]
    num = models.TextField(_("Key num"))
    status = models.CharField(_("Key status"), max_length=1, choices=KEY_STATUS, default="A")
    teacher = models.ForeignKey("Teacher", verbose_name=_("Teacher who take key"), on_delete=models.CASCADE, null=True)
    

    class Meta:
        verbose_name = _("Key")
        verbose_name_plural = _("Keys")

    def __str__(self):
        return self.num


class KeyHistory(models.Model):
    EVENT_TYPES = [
        ("L", "Leave"),
        ("T", "Take"),
    ]
    datetime = models.DateTimeField(_("Event time"), auto_now=True, auto_now_add=False)
    key = models.ForeignKey("Key", verbose_name=_("Event key"), on_delete=models.CASCADE)
    teacher = models.ForeignKey("Teacher", verbose_name=_("Event teacher"), on_delete=models.CASCADE)
    event_type = models.CharField(_("Event type"), max_length=1, choices=EVENT_TYPES, default="T")
    

    class Meta:
        verbose_name = _("KeyHistory")
        verbose_name_plural = _("KeyHistories")

    def __str__(self):
        return f'{self.datetime} | {self.key} | {self.teacher}'

from django.shortcuts import render
from rest_framework import viewsets
from django_tables2 import SingleTableView

from API.models import Teacher, KeyHistory, Key, Card
from API.serializers import TeacherSerializer, KeyHistorySerializer, KeySerializer, CardSerializer
from API.tables import HistoryTable, KeyTable


class TeacherViewSet(viewsets.ModelViewSet):
    queryset = Teacher.objects.all().order_by('pk')
    serializer_class = TeacherSerializer


class CardViewSet(viewsets.ModelViewSet):
    queryset = Card.objects.all().order_by('pk')
    serializer_class = CardSerializer


class KeyViewSet(viewsets.ModelViewSet):
    queryset = Key.objects.all().order_by('pk')
    serializer_class = KeySerializer


class KeyHistoryViewSet(viewsets.ModelViewSet):
    queryset = KeyHistory.objects.all().order_by('pk')
    serializer_class = KeyHistorySerializer


class HistoryListView(SingleTableView):
    model = KeyHistory
    table_class = HistoryTable
    template_name = 'API/history.html'


class KeyListView(SingleTableView):
    model = Key
    table_class = KeyTable
    template_name = 'API/status.html'